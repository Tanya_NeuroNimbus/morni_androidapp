package com.example.user.morni_androidapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import utils.Country;

public class CountryListArrayAdapter extends ArrayAdapter<Country> {

    private final List<Country> list;
    private final Activity context;

    static class ViewHolder {
        protected TextView name;
        protected TextView text;
        protected ImageView flag;
    }

    public CountryListArrayAdapter(Activity context, List<Country> list) {
        super(context, R.layout.country_code_row, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.country_code_row, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.name);
            viewHolder.text = (TextView) view.findViewById(R.id.textView);

            view.setTag(viewHolder);
        } else {
            view = convertView;
        }


        ViewHolder holder = (ViewHolder) view.getTag();
        Pattern intsOnly = Pattern.compile("\\d+");
        String numberRefined= list.get(position).getCode().replaceAll("[0-9]", "").replace(",","");
        Matcher makeMatch = intsOnly.matcher(list.get(position).getCode());
        makeMatch.find();
        String result = makeMatch.group();
        System.out.println("VALUES____>" + numberRefined);
        holder.name.setText(result);

        holder.text.setText(numberRefined);

        return view;
    }

}