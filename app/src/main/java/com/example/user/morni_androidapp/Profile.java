package com.example.user.morni_androidapp;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.user.morni_androidapp.Asset.RobotoButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import utils.Pref_Model;
import utils.Urls_Morni;

public class Profile extends AppCompatActivity {

    TextView edit_phonenum;
    EditText edit_firstname, edit_lastname, email_id_edit;
    String first_name_str, last_name_str, email_str;
    StringEntity stringEntity;
    AsyncHttpClient client;
    RobotoButton update_btn;
//    ImageView avatar_iv;
//    username_tv, lang_name_tv

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        client = new AsyncHttpClient();

        edit_phonenum = (TextView)findViewById(R.id.edit_phonenum);
//        lang_name_tv = (TextView)findViewById(R.id.lang_name_tv);
//        username_tv = (TextView)findViewById(R.id.username_tv);
        edit_firstname = (EditText)findViewById(R.id.edit_firstname);
        edit_lastname = (EditText)findViewById(R.id.edit_lastname);
        email_id_edit = (EditText)findViewById(R.id.email_id_edit);
        update_btn = (RobotoButton)findViewById(R.id.update_btn);
//        avatar_iv = (ImageView)findViewById(R.id.avatar_iv);

//        edit_phonenum.setText(getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
//                .getString(Pref_Model.User_Info.user_phonenum, ""));

//        username_tv.setText(getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
//                .getString(Pref_Model.User_Info.user_phonenum, ""));

//        lang_name_tv.setText(getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
//                .getString(Pref_Model.User_Info.user_lang, ""));



        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name_str = edit_firstname.getText().toString();
                last_name_str = edit_lastname.getText().toString();
                email_str = email_id_edit.getText().toString();

                update_user();
            }
        });


    }

    public void update_user() {

        StringEntity stringEntity = null;
        // final String url = Urls_Morni.login_url;

        JSONObject object = new JSONObject();
        JSONObject object2 = new JSONObject();

        try {

            object2.put("last_name", last_name_str);
            object2.put("email", email_str);
            object2.put("first_name", first_name_str);
            object.put("user", object2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("auth_token" + object);


        try {
            stringEntity = new StringEntity(object.toString());
            Log.e("check for body", object.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.addHeader("Content-Type", "application/json");
        stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        client.put(getApplicationContext(),Urls_Morni.update_user_url,stringEntity,"application/json",new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                Log.e("Response", s);



            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                System.out.println("Responce-->error" + s);

            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }
}
