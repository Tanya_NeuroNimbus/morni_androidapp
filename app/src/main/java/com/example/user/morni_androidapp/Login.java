package com.example.user.morni_androidapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import utils.MarshmallowClient;
import utils.Urls_Morni;

public class Login extends AppCompatActivity {

    ProgressBar progressBar;
    EditText edit_email, edit_password;
    Button login_btn;

    StringEntity stringEntity;
    String username_str, password_str;

    AsyncHttpClient cli;

    public static String TAG = Login.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        cli = new AsyncHttpClient();

        edit_password = (EditText)findViewById(R.id.edit_password);
        edit_email = (EditText)findViewById(R.id.edit_email);
        login_btn = (Button)findViewById(R.id.login_btn);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username_str = edit_email.getText().toString();
                password_str = edit_password.getText().toString();

                if(username_str==null||username_str.equalsIgnoreCase("")||password_str==null||password_str.equalsIgnoreCase("")){

                    Toast.makeText(Login.this, "Please enter your credentials", Toast.LENGTH_SHORT).show();
                }else{

                 //   new Login_User().execute(Urls_Morni.login_url);

                    Login_user();
                }

            }
        });


    }

    private class Login_User extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MarshmallowClient client = new MarshmallowClient(params[0]);

                JSONObject object = new JSONObject();
                JSONObject object2 = new JSONObject();
                object.put("username" , username_str);
                object.put("password", password_str);
                object2.accumulate("user_session", object);

                client.AddParam("username", username_str);

                return client.Execute(MarshmallowClient.RequestMethod.POST);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Response", s);


            try {

                JSONObject jsonObject = new JSONObject(s);

                String status= jsonObject.getString("status");
                String code= jsonObject.getString("code");

                if(status.equalsIgnoreCase("success")) {

                    JSONObject object = jsonObject.getJSONObject("data");



                        String id = object.getString("id");
                        String first_name = object.getString("first_name");
                        String last_name = object.getString("last_name");
                        String phone_number = object.getString("phone_number");
                        String single_access_token = object.getString("single_access_token");
                        String created_at = object.getString("created_at");
                        String updated_at = object.getString("updated_at");

                        Toast.makeText(Login.this, "Succesfully Logged in", Toast.LENGTH_SHORT).show();


                }else{
                    JSONArray jsonArray = jsonObject.getJSONArray("message");

                    Toast.makeText(Login.this, "Password is incorrect", Toast.LENGTH_SHORT).show();

                }


                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);


            } catch (JSONException e) {
                Toast.makeText(Login.this, "No Data From Server", Toast.LENGTH_SHORT).show();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }

        }
    }
    public void Login_user() {


        final String url = Urls_Morni.login_url;

        JSONObject object = new JSONObject();
        JSONObject object2 = new JSONObject();

//        RequestParams params = new RequestParams();
//        params.


        try {
            object2.put("username" , username_str);
            object2.put("password", password_str);
            object.accumulate("user_session", object2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("auth_token" + object2);


        try {
            stringEntity = new StringEntity(object.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        stringEntity.setContentType("application/json");
        cli.addHeader("ContentType", "application/json");

        //    System.out.println("url----->>>>>>>"+url+params);
        cli.post(getApplicationContext(),url,stringEntity,"application/json",new AsyncHttpResponseHandler(){
            @Override
            public void onStart() {
                super.onStart();


                progressBar.setIndeterminate(true);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                System.out.println("Responce-->" + s);

                Toast.makeText(Login.this, "Logged in successfully", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(Login.this, Profile.class);
                startActivity(i);
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

                 try {

                JSONObject jsonObject = new JSONObject(s);

                String status= jsonObject.getString("status");
                String code= jsonObject.getString("code");

                if(status.equalsIgnoreCase("success")) {

                    JSONObject object = jsonObject.getJSONObject("data");



                    String id = object.getString("id");
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String phone_number = object.getString("phone_number");
                    String single_access_token = object.getString("single_access_token");
                    String created_at = object.getString("created_at");
                    String updated_at = object.getString("updated_at");

                    Toast.makeText(Login.this, "Succesfully Logged in", Toast.LENGTH_SHORT).show();


                }else{
                    JSONArray jsonArray = jsonObject.getJSONArray("message");

                    Toast.makeText(Login.this, "Password is incorrect", Toast.LENGTH_SHORT).show();

                }


                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);


            } catch (JSONException e) {
                Toast.makeText(Login.this, "No Data From Server", Toast.LENGTH_SHORT).show();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }


            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                System.out.println("Responce-->error" + s);
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

            }
        });
    }



}
