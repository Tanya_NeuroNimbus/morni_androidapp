package com.example.user.morni_androidapp;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.internal.NavigationMenu;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import utils.MarshmallowClient;
import utils.Pref_Model;
import utils.Urls_Morni;

public class Registration extends Navigation_Menu {


    ProgressBar progressBar;
    EditText edit_email, edit_password, edit_confirm_password;
    String username_str, password_str, confirm_password_str, otp_str;
    Button register_btn;
    Spinner spinner;
    static  String lang_str, lang_tosend;
    StringEntity stringEntity;
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration2);
        client = new AsyncHttpClient();

        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        edit_confirm_password = (EditText)findViewById(R.id.edit_confirm_password);
        edit_password = (EditText)findViewById(R.id.edit_password);
        edit_email = (EditText)findViewById(R.id.edit_email);
        register_btn = (Button)findViewById(R.id.register_btn);

        edit_email.setText(getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
                .getString(Pref_Model.User_Info.user_phonenum, ""));


         spinner = (Spinner) findViewById(R.id.spinner);
        List<String> language = new ArrayList<String>();
        language.add("Arabic");
        language.add("English");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, language);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               lang_str = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username_str = edit_email.getText().toString();
                password_str = edit_password.getText().toString();
                confirm_password_str = edit_confirm_password.getText().toString();
                if(lang_str.equalsIgnoreCase("English")){
                    lang_tosend = "en";
                }else{
                    lang_tosend = "ar";
                }


                if(username_str==null||username_str.equalsIgnoreCase("")||password_str==null||password_str.equalsIgnoreCase("")||
                        confirm_password_str==null||confirm_password_str.equalsIgnoreCase("")){
                    Toast.makeText(Registration.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                }else{

                    if(confirm_password_str.equalsIgnoreCase(password_str)){
                       Register_user();

                    }else{
                        Toast.makeText(Registration.this, "Please enter your password correctly", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


    }


//    private class Register_User extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressBar.setIndeterminate(true);
//            progressBar.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                MarshmallowClient client = new MarshmallowClient(params[0]);
//
//                client.AddParam("username", username_str);
//                client.AddParam("phone_number", getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
//                        .getString(Pref_Model.User_Info.user_phonenum, ""));
//                client.AddParam("password", password_str);
//                client.AddParam("password_confirmation", confirm_password_str);
//                client.AddParam("language", lang_tosend);
//                return client.Execute(MarshmallowClient.RequestMethod.POST);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            Log.e("Response", s);
//
//
//            try {
//
//                JSONObject jsonObject = new JSONObject(s);
//
//                String status= jsonObject.getString("status");
//                String code= jsonObject.getString("code");
//
//                if(status.equalsIgnoreCase("success")) {
//
//                    JSONObject object = jsonObject.getJSONObject("data");
//
//
//                        String id = object.getString("id");
//                        String first_name = object.getString("first_name");
//                        String last_name = object.getString("last_name");
//                        String phone_number = object.getString("phone_number");
//                        String email = object.getString("email");
//                        String created_at = object.getString("created_at");
//                        String updated_at = object.getString("updated_at");
//                        String language = object.getString("language");
//                        String avatar_thumb = object.getString("avatar_thumb");
//                        String avatar_medium = object.getString("avatar_medium");
//                        String unread_management_messages = object.getString("unread_management_messages");
//                        String unread_service_requests = object.getString("unread_service_requests");
//                        JSONArray roles = jsonObject.getJSONArray("roles");
//
//
//
//                    Intent i = new Intent(Registration.this, Login.class);
//                    startActivity(i);
//                }else{
//                    Toast.makeText(Registration.this, "Username has already been taken", Toast.LENGTH_SHORT).show();
//                }
//
//
//                if (progressBar.isShown())
//                    progressBar.setVisibility(View.GONE);
//
//
//
//            } catch (JSONException e) {
//                Toast.makeText(Registration.this, "No Data From Server", Toast.LENGTH_SHORT).show();
//                if (progressBar.isShown())
//                    progressBar.setVisibility(View.GONE);
//                e.printStackTrace();
//            }
//
//        }
//    }

    private class Get_OTP extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MarshmallowClient client = new MarshmallowClient(params[0]);

                client.AddParam("phone_number", getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
                        .getString(Pref_Model.User_Info.user_phonenum, ""));
                client.AddParam("sms_key", "aDfiduB63T9ZeDUt90zhloTlmzt46KUe");
                client.AddParam("phone_verification_code", otp_str);
                return client.Execute(MarshmallowClient.RequestMethod.POST);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Response", s);


            try {
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

                JSONObject jsonObject = new JSONObject(s);


                String code= jsonObject.getString("code");

                if(code.equalsIgnoreCase("ok")) {

                    String status= jsonObject.getString("status");
                    Intent i = new Intent(Registration.this, Login.class);
                    startActivity(i);
                }else if(code.equalsIgnoreCase("unprocessable_entity")){


                    String message = jsonObject.getString("message");


                    Toast.makeText(Registration.this, message, Toast.LENGTH_SHORT).show();

                }else if(code.equalsIgnoreCase("unauthorized")){

                    String message = jsonObject.getString("message");


                    Toast.makeText(Registration.this, message, Toast.LENGTH_SHORT).show();

                }



            } catch (JSONException e) {
                Toast.makeText(Registration.this, "No Data From Server", Toast.LENGTH_SHORT).show();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }

        }
    }

    public void Register_user() {



        StringEntity stringEntity = null;
        // final String url = Urls_Morni.login_url;

        JSONObject object = new JSONObject();
        JSONObject object2 = new JSONObject();

        try {

            object2.put("username" , username_str);
            object2.put("password", password_str);
            object2.put("phone_number", getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE)
                    .getString(Pref_Model.User_Info.user_phonenum, ""));
            object2.put("password_confirmation", confirm_password_str);
            object2.put("language", lang_tosend);
            object.put("user", object2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("auth_token" + object);


        try {
            stringEntity = new StringEntity(object.toString());
            Log.e("check for body", object.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.addHeader("Content-Type", "application/json");
        stringEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        client.post(getApplicationContext(),Urls_Morni.register_url,stringEntity,"application/json",new AsyncHttpResponseHandler(){
            @Override
            public void onStart() {
                super.onStart();


                progressBar.setIndeterminate(true);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                Log.e("Response", s);

                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

                savelangToPref(lang_tosend);

                            try {

                JSONObject jsonObject = new JSONObject(s);

                String status= jsonObject.getString("status");
                String code= jsonObject.getString("code");

                if(status.equalsIgnoreCase("success")) {

                    JSONObject object = jsonObject.getJSONObject("data");


                        String id = object.getString("id");
                        String first_name = object.getString("first_name");
                        String last_name = object.getString("last_name");
                        String phone_number = object.getString("phone_number");
                        String email = object.getString("email");
                        String created_at = object.getString("created_at");
                        String updated_at = object.getString("updated_at");
                        String language = object.getString("language");
                        String avatar_thumb = object.getString("avatar_thumb");
                        String avatar_medium = object.getString("avatar_medium");
                        String unread_management_messages = object.getString("unread_management_messages");
                        String unread_service_requests = object.getString("unread_service_requests");
//                        JSONArray roles = jsonObject.getJSONArray("roles");



                    Intent i = new Intent(Registration.this, Login.class);
                    startActivity(i);
                }else{
                    Toast.makeText(Registration.this, "Username has already been taken", Toast.LENGTH_SHORT).show();
                }


                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);



            } catch (JSONException e) {
                Toast.makeText(Registration.this, "No Data From Server", Toast.LENGTH_SHORT).show();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }

                final Dialog dialog = new Dialog(Registration.this);
                dialog.setContentView(R.layout.otp_dalog);
                dialog.setTitle("Enter the OTP...");

                // set the custom dialog components - text, image and button
               final EditText text = (EditText) dialog.findViewById(R.id.text);

                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new Get_OTP().execute(Urls_Morni.otp_url);
                        otp_str = text.getText().toString();
                        dialog.dismiss();

                    }
                });

                dialog.show();



            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                System.out.println("Responce-->error" + s);
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void savelangToPref(String status) {
        SharedPreferences pref = getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Pref_Model.User_Info.user_lang, status);
        editor.commit();
    }


}
