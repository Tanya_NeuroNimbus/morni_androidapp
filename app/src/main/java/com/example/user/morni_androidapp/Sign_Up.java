package com.example.user.morni_androidapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.morni_androidapp.Asset.RobotoButton;
import com.example.user.morni_androidapp.Asset.RobotoEditText;
import com.example.user.morni_androidapp.Asset.RobotoTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import utils.Country;
import utils.MarshmallowClient;
import utils.Pref_Model;

public class Sign_Up extends AppCompatActivity {

    String[] countrycodes;
    List<Country> countryList;
    PopupWindow popupWindow;
    RobotoEditText edit_number;
    RobotoButton otp_btn;
    ImageView drop_iv;
    RobotoTextView code_tv;
    ProgressBar progressBar;
    String number_str;
    ImageButton back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup__screen);


        edit_number = (RobotoEditText)findViewById(R.id.edit_number);
        otp_btn = (RobotoButton)findViewById(R.id.otp_btn);
        drop_iv = (ImageView)findViewById(R.id.drop_iv);
        code_tv = (RobotoTextView)findViewById(R.id.code_tv);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        ImageButton menu_btn = (ImageButton)findViewById(R.id.menu_button);
        menu_btn.setVisibility(View.INVISIBLE);
        back_btn = (ImageButton)findViewById(R.id.back_button);
        back_btn.setVisibility(View.GONE);

        ImageButton profile_button = (ImageButton)findViewById(R.id.profile_button);
        profile_button.setVisibility(View.INVISIBLE);

        drop_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                discardPermission();
            }
        });
        otp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                number_str = code_tv.getText().toString() + edit_number.getText().toString();
//
//                new Check_User().execute();

                Intent i = new Intent(Sign_Up.this, Dashboard.class);
                startActivity(i);
            }
        });

    }

    private class Check_User extends AsyncTask<String, Void, String>{


        @Override
        protected String doInBackground(String... params) {
            try {
                MarshmallowClient client = new MarshmallowClient("https://morni-v3.herokuapp.com//api/app_users/look_up"+number_str);

                return client.Execute(MarshmallowClient.RequestMethod.GET);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String s) {

            try {

                JSONObject jsonObject = new JSONObject(s);

                String status= jsonObject.getString("status");
                String code= jsonObject.getString("code");

                if(status.equalsIgnoreCase("success")) {




                    Toast.makeText(Sign_Up.this, "User already exist", Toast.LENGTH_SHORT).show();

                }else{

                    savePhoneNumToPref(number_str);

                  Intent i = new Intent(Sign_Up.this, Registration.class);
                    startActivity(i);
                }


                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);


            } catch (JSONException e) {
                Toast.makeText(Sign_Up.this, "No Data From Server", Toast.LENGTH_SHORT).show();
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.VISIBLE);
        }
    }



    private void discardPermission() {

        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        View popupView = layoutInflater.inflate(R.layout.new_popup, (ViewGroup) findViewById(R.id.new_view));

        countryList = new ArrayList<Country>();
        countrycodes = getResources().getStringArray(R.array.country_codes);

        for(int i = 0; i < countrycodes.length; i++){
            System.out.println("THIs Values--->" + countryList.size());
            countryList.add(new Country(countrycodes[i]));
            System.out.println("THIs Values--->" + countryList.size());
        }
        System.out.println("THIs Values--->" + countryList.size());

        popupWindow = new PopupWindow();

//        popupWindow.setAnimationStyle(R.style.DialogAnimation);

        popupWindow.setContentView(popupView);

        popupWindow.setHeight(RelativeLayout.LayoutParams.MATCH_PARENT);

        popupWindow.setWidth(RelativeLayout.LayoutParams.WRAP_CONTENT);

        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        popupWindow.setFocusable(true);

        popupWindow.setTouchable(true);

        popupWindow.update();

        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);

        ListView view = (ListView) popupView.findViewById(R.id.country_list);

        ImageButton imageButton= (ImageButton)popupView.findViewById(R.id.pop_can);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        ArrayAdapter<Country> adapter = new CountryListArrayAdapter(Sign_Up.this, countryList);
        view.setAdapter(adapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Country c = countryList.get(position);
                String str = c.getCode().replaceAll("\\D+", "");

                code_tv.setText("+" + str);
                // setResult(RESULT_OK, returnIntent);
                popupWindow.dismiss();
            }
        });


    }



    public void savePhoneNumToPref(String status) {
        SharedPreferences pref = getSharedPreferences(Pref_Model.User_Info.EMAIL_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Pref_Model.User_Info.user_phonenum, status);
        editor.commit();
    }

}
