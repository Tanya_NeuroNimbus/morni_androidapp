package com.example.user.morni_androidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.aakira.expandablelayout.ExpandableWeightLayout;

public class FAQ_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq__screen);

        ExpandableWeightLayout expandableLayout
                = (ExpandableWeightLayout) findViewById(R.id.expandableLayout1);

// toggle expand, collapse
        expandableLayout.toggle();
// expand
        expandableLayout.expand();
// collapse
        expandableLayout.collapse();
    }
}
