package utils;


public class Pref_Model {

    public static class User_Info{

        public static final String EMAIL_PREF = "email_pref";
        public static final String user_phonenum = "user_phonenum";
        public static final String user_avatar = "user_avatar";
        public static final String user_lang = "user_lang";
        public static final String user_emailid = "user_emailid";
        public static final String user_firstname = "user_firstname";
        public static final String user_lastname = "user_lastname";

    }

}
