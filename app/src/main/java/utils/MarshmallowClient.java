package utils;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MarshmallowClient {

    private int statusCode;
    private String url;
    DataOutputStream printout;
    JSONObject jsonParam = new JSONObject();

    public MarshmallowClient(String url) {
        Log.e("Marshmallow Client", "constructor called");
        this.url = url;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String Execute(RequestMethod method) throws Exception {

        Log.e("Marshmallow Client", "execute method");
        StringBuilder stringBuilder = new StringBuilder();

        switch (method) {
            case GET: {
                Log.e("request", "GET");
                try {
                    URL urlObject = new URL(url);
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObject.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setConnectTimeout(5000);
                    urlConnection.setReadTimeout(5000);
                    urlConnection.connect();

                    statusCode = urlConnection.getResponseCode();
                    Log.e("response code", String.valueOf(statusCode));

                    if (statusCode == 200) {
                        InputStream inputStream = urlConnection.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                    } else {
                        Log.e("Marshmallow Json", "Failed to Download File, Response code != 200");
                    }
                } catch (IOException e) {
                    Log.e("Marshmallow exception", "Exception occured:" + e.getMessage());
                }
                break;
            }

            case POST: {
                Log.e("request", "POST");
                try {
                    URL urlObject = new URL(url);
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObject.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setConnectTimeout(5000);
                    urlConnection.setReadTimeout(5000);

                    urlConnection.connect();

                    printout = new DataOutputStream(urlConnection.getOutputStream());
                    printout.writeBytes(jsonParam.toString());
                    printout.flush();
                    printout.close();

                    statusCode = urlConnection.getResponseCode();
                    Log.e("response code", String.valueOf(statusCode));

                    if (statusCode == 200) {
                        InputStream inputStream = urlConnection.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                    } else {
                        Log.e("Marshmallow Json", "Failed to Download File, Response code != 200");
                    }
                } catch (IOException e) {
                    Log.e("Marshmallow exception", "Exception occured:" + e.getMessage());
                }
                break;
            }
        }
        Log.e("Marshmallow Response",stringBuilder.toString());
        return stringBuilder.toString();
    }

    public void AddParam(String name, String value) throws JSONException {
        jsonParam.put(name, value);
        Log.e("Marshmallow Json", jsonParam.toString());
    }

    public enum RequestMethod {
        GET,
        POST
    }
}