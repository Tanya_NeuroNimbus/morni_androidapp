package utils;

/**
 * Created by user on 20/5/16.
 */
public class Urls_Morni {

    public static String Base_url = "http://morni-v3.herokuapp.com";

    public static String register_url = Base_url + "/api/app_users";

    public static String login_url = Base_url + "/api/login";

    public static String otp_url = Base_url + "/api/app_users/verify_phone_number";

    public static String update_user_url = Base_url + "/api/app_users/update";

    public static String update_avatar_url = Base_url + "/apiapi/app_users/update_avatar";

}
